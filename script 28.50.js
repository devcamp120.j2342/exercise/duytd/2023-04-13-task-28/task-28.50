"use strict";
$(document).ready(function(){
    //REGION 1

    //REGION 2
    //gán sự kiện nút bấm submitData
    $("#btn-submit-data").on('click', onBtnSubmitDataClick);
    //gán sự kiện nút bấm ClearHtmlLog
    $("#btn-clear-log").on('click', onBtnClearHtmlLogClick);
    //REGION 3
    
    //hàm thực hiện sự kiện nút bấm SubmitData
    function onBtnSubmitDataClick(){
        "use strict";
        //khai báo đối tượng chứa dữ liệu
        var vPersonObj = {
            firstName: "",
            lastName: "",
            age: ""
        }
        //b1: thu thập dữ liệu
        getData(vPersonObj);
        console.log("%cAge: " + vPersonObj.age, "color: purple");
        console.log("%cFirstName: " + vPersonObj.firstName, "color: blue");
        console.log("%cLastName: " + vPersonObj.lastName, "color:green");
        //b2: kiểm tra dữ liệu
        var vIsCheck = validateData(vPersonObj);
        if(vIsCheck){
            //b4: hiển thị thông tin
            displayInformation(vPersonObj);
        }
    }
    //hàm thực hiện sự kiện nút bấm clear HTML log
    function onBtnClearHtmlLogClick(){
        console.log("%cNút Clear Html Click được bấm","color: red");
        deleteLog();
    }
    //REGION 4
    //hàm thu thập dữ liệu
    
    function getData(paramObj){
        "use strict";
        var vElementFirstName = $("#inp-first-name");
        paramObj.firstName = vElementFirstName.val();

        var vElementLastName = $("#inp-last-name");
        paramObj.lastName = vElementLastName.val();

        var vElementAge = $("#inp-age");
        paramObj.age = vElementAge.val();
    }
    //hàm kiểm tra dữ liệu
    function validateData(paramObj){
        "use strict";
        if(paramObj.firstName == ""){
            console.log("dữ liêu không họp lệ")
            return false;
        }
        if(paramObj.lastName == ""){
            console.log("dữ liêu không họp lệ")
            return false;
        }
        if(!validateAge(paramObj.age)){
            return false;
        }
        return true;
    }
    //hàm kiểm tra tuổi
    function validateAge(paramAge){
        "use strict";
        if(paramAge == ""){
            console.log("dữ liêu không họp lệ");
            return false;
        }
        //nếu tuổi nhập vào không phải là số
        if(isNaN(paramAge)){
            console.log("dữ liêu không họp lệ")
            return false;
        }
        paramAge = parseInt(paramAge); //chuyển paramAge về number
        if(paramAge <= 0 || paramAge > 200){ //nếu tuổi nhập vào nhỏ hơn 0 và lớn hơn 200 => flase;
            console.log("dữ liêu không họp lệ")
            return false;
        }
        return true;
    }
    //hàm hiển thị thông tin
    function displayInformation(paramObj){
        var vElementP = $("#p-html-log");
        console.log("id thẻ p: " + vElementP.attr('id'));
        vElementP.html("> " + "Age: " + paramObj.age + "<br>");
        vElementP.append("> " + "FirstName: " + paramObj.firstName + "<br>");
        vElementP.append("> " + "LastName: " + paramObj.lastName);
    }
    //hàm thực hiện xóa toàn bộ log ở vùng 2
    function deleteLog(){
        var vElementP = $("#p-html-log");
        vElementP.html('>');
    }
})